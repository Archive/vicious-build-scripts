# This is for building the development GNOME (from CVS HEAD)

# This file should be sourced in

MODULES='
	esound
	gnome-xml
	libxslt
	libxklavier
	gtk-doc
	glib
	intltool
	shared-mime-info
	gnome-common
	atk
	pango
	libIDL
	ORBit2
	gtk+
	gconf
	libart_lgpl
	vte
	libbonobo
	gnome-mime-data
	gnome-keyring
	gnome-vfs
	gstreamer
	gst-plugins
	libglade
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	gnome-icon-theme
	libgnomeprintui
	libgda
	libgnomedb
	libgtop
	libgsf
	libcroco
	librsvg
	gail
	eel
	gtkhtml2
	gtk-engines
	gnome-desktop
	gnome-panel
	gnome-session
	gnome-terminal
	gnome-utils
	gnome-applets
        metacity
	nautilus	
	nautilus-cd-burner
	nautilus-media
	gnome-control-center
	glade
	gnome-games
	bug-buddy
	eog
	procman
	yelp
	gtksourceview
	gedit
	gnome-media
	gnome-themes
	at-spi
	libgail-gnome
	at-poke
	gnome-mag
	gnome-speech
	gnopernicus
	acme
	file-roller
	gcalctool
	gconf-editor
	gpdf
	gucharmap
	totem
	zenity
	dasher
	gnome-netstatus
	ggv
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# CVSFLAGS_atk: -r gnome-2-6
# CVSFLAGS_glib: -r glib-2-4
# CVSFLAGS_gtk+: -r gtk-2-4
# CVSFLAGS_gconf: -r gnome-2-6
# CVSFLAGS_gnome-icon-theme: -r gnome-2-6
# CVSFLAGS_gnome-vfs: -r gnome-2-6
# CVSFLAGS_gnome-keyring: -r gnome-2-6
# CVSFLAGS_libgnome: -r gnome-2-6
# CVSFLAGS_libgnomeui: -r gnome-2-6
# CVSFLAGS_libgnomecanvas: -r gnome-2-6
# CVSFLAGS_bug-buddy: -r gnome-2-6
# CVSFLAGS_gnome-games: -r gnome-2-6
# CVSFLAGS_libcroco: -r gnome-2-6
# CVSFLAGS_librsvg: -r gnome-2-6
# CVSFLAGS_nautilus-cd-burner: -r gnome-2-6
# CVSFLAGS_gnome-control-center: -r gnome-2-6
# CVSFLAGS_yelp: -r gnome-2-6
# CVSFLAGS_gconf-editor: -r gnome-2-6
# CVSFLAGS_gnome-terminal: -r gnome-2-6
# CVSFLAGS_libgnomeprint: -r gnome-2-6
# CVSFLAGS_libgnomeprintui: -r gnome-2-6
# CVSFLAGS_gedit: -r gnome-2-6
# CVSFLAGS_eog: -r gnome-2-6
# CVSFLAGS_file-roller: -r gnome-2-6
# CVSFLAGS_gcalctool: -r gnome-2-6
# CVSFLAGS_ggv: -r gnome-2-6
# CVSFLAGS_gtksourceview: -r gnome-2-6
# CVSFLAGS_at-spi: -r gnome-2-6
# CVSFLAGS_gok: -r gnome-2-6
# CVSFLAGS_gnome-mag: -r gnome-2-6
# CVSFLAGS_gnopernicus: -r gnome-2-6
# CVSFLAGS_dasher: -r gnome-2-6
# CVSFLAGS_gnome-themes: -r gnome-2-6
# CVSFLAGS_gnome-netstatus: -r gnome-2-6
# CVSFLAGS_gnome-media: -r gnome-2-6
# CVSFLAGS_gnome-panel: -r gnome-2-6
# CVSFLAGS_gnome-desktop: -r gnome-2-6
# CVSFLAGS_gnome-session: -r gnome-2-6
# AUTOGEN_glib: --enable-debug=yes
# AUTOGEN_gtk+: --enable-debug=yes
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# AUTOGEN_libgnomeprint: --disable-gtk-doc
# AUTOGEN_libgnomeprintui: --disable-gtk-doc
# AUTOGEN_libgsf: --disable-gtk-doc
# AUTOGEN_librsvg: --disable-gtk-doc
# AUTOGEN_libgda: --with-postgres=yes --with-mysql=yes --with-oracle=yes --with-sqlite=yes --with-odbc=yes --with-mdb=yes --disable-gtk-doc
# AUTOGEN_ORBit2: --enable-debug
# CVSFLAGS_glade: -r glade-gnome2-branch
# CVSROOT_gstreamer: :pserver:anoncvs@cvs.freedesktop.org:/cvs/gstreamer
# AUTOGEN_gstreamer: -- --disable-plugin-builddir --disable-docs-build
# CVSROOT_gst-plugins: :pserver:anoncvs@cvs.freedesktop.org:/cvs/gstreamer
# AUTOGEN_totem: --enable-gstreamer
# CVSROOT_libxklavier: :pserver:anoncvs@cvs.freedesktop.org:/cvs/xklavier
# CVSROOT_shared-mime-info: :pserver:anoncvs@cvs.freedesktop.org:/cvs/mime
# AUTOGEN_dasher: --with-a11y --with-gnome
