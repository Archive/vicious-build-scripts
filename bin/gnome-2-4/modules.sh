# This is for building the released GNOME 2.4 modules

# This file should be sourced in

MODULES='
	esound
	gnome-xml
	libxslt
	gtk-doc
	glib
	gnome-common
	atk
	pango
	libIDL
	ORBit2
	intltool
	gtk+
	gconf
	libart_lgpl
	libzvt
	vte
	libbonobo
	gnome-mime-data
	gnome-vfs
	gstreamer
	gst-plugins
	libglade
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	libgnomeprintui
	libgda
	libgnomedb
	libgtop
	libgsf
	libcroco
	librsvg
	gail
	eel
	gtkhtml2
	gtk-engines
	gnome-desktop
	gnome-panel
	gnome-session
	gnome-terminal
	gnome-utils
	gnome-applets
        metacity
	nautilus	
	nautilus-cd-burner
	nautilus-media
	gnome-control-center
	glade
	gnome-games
	bug-buddy
	eog
	procman
	yelp
	gtksourceview
	gedit
	gnome-media
	gnome-vfs-extras
	thinice2
	gnome-icon-theme
	gnome-themes
	at-spi
	libgail-gnome
	at-poke
	gnome-mag
	gnome-speech
	gnopernicus
	acme
	file-roller
	gcalctool
	gconf-editor
	gpdf
	gucharmap
	zenity
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# AUTOGEN_gnumeric: --without-bonobo
# AUTOGEN_libbonobo: --enable-compile-warnings=maximum
# AUTOGEN_libbonoboui: --enable-compile-warnings=maximum
# AUTOGEN_libgda: --with-postgres=yes --with-mysql=yes --with-oracle=yes --with-sqlite=yes --with-odbc=yes --with-mdb=yes
# CVSFLAGS_libgda: -r release-1-0-branch
# CVSFLAGS_libgnomedb: -r release-1-0-branch
# AUTOGEN_libgnomecanvas: --enable-compile-warnings=error
# AUTOGEN_libgnome: --enable-compile-warnings=error
# AUTOGEN_libgnomeui: --enable-compile-warnings=maximum
# AUTOGEN_ORBit2: --enable-debug
# AUTOGEN_sawfish:  --with-gdk-pixbuf
# CVSFLAGS_eel: -r gnome-2-4
# CVSFLAGS_gconf-editor: -r gnome-2-4
# CVSFLAGS_gedit: -r gnome-2-4
# CVSFLAGS_glade: -r glade-gnome2-branch
# CVSFLAGS_glib: -r glib-2-2
# AUTOGEN_glib: --enable-debug=yes
# CVSFLAGS_gnome-applets: -r gnome-2-4
# CVSFLAGS_gnome-control-center: -r gnome-2-4
# CVSFLAGS_gnome-games: -r gnome-2-4
# CVSFLAGS_gnome-mime-data: -r gnome-2-4
# CVSFLAGS_gnome-panel: -r gnome-2-4
# CVSFLAGS_gnome-themes: -r gnome-2-4
# CVSFLAGS_gnome-utils: -r gnome-2-4
# CVSFLAGS_gnome-vfs: -r gnome-2-4
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# CVSFLAGS_gst-plugins: -r BRANCH-GSTREAMER-0_6
# CVSFLAGS_gstreamer: -r BRANCH-GSTREAMER-0_6
# AUTOGEN_gstreamer: -- --disable-plugin-builddir
# CVSFLAGS_gtk+: -r gtk-2-2
# AUTOGEN_gtk+: --enable-debug=yes
# CVSFLAGS_libgtop: -r libgtop-GNOME-2-0-port
# CVSFLAGS_librsvg: -r gnome-2-4
# CVSFLAGS_nautilus: -r gnome-2-4
# CVSFLAGS_nautilus_cd_burner: -r gnome-2-4
# CVSFLAGS_pango: -r pango-1-2
# CVSFLAGS_sawfish: -r gnome-2
# CVSFLAGS_yelp: -r gnome-2-4
# CVSROOT_gst-plugins: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# CVSROOT_gstreamer: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# CVSROOT_thinice2: :pserver:anonymous@cvs.sourceforge.net:/cvsroot/thinice
# CVSFLAGS_gnome-media: -r gnome-2-4
# CVSFLAGS_gnome-xml: -r LIBXML2_2_5_X
# CVSFLAGS_acme: -r gnome-2-4
