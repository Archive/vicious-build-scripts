#!/usr/bin/env zsh

source $HOME/bin/check-env.sh ${0:t}

echo -----------------
echo Checking out
echo -----------------

echo cd $PREFIX/cvs
cd $PREFIX/cvs
source setvars.sh

if [ ! -d vicious-build-scripts ]; then
	echo $CVSPREFIX cvs -z3 co vicious-build-scripts
	if ! $CVSPREFIX cvs -z3 co vicious-build-scripts ; then
		echo
		echo ERROR: checking out vicious-build-scripts
		echo
		exit 1
	fi
	echo cd vicious-build-scripts
	cd vicious-build-scripts
else
	echo cd vicious-build-scripts
	cd vicious-build-scripts
	echo $CVSPREFIX cvs -z3 update -dP
	if ! $CVSPREFIX cvs -z3 update -dP ; then
		echo
		echo ERROR: updating vicious-build-scripts
		echo
		exit 1
	fi
fi

echo -----------------
echo Installing
echo -----------------

echo make install
if ! make install ; then
	echo
	echo ERROR: Cant install
	echo
	exit 1
fi

echo
echo "Scripts updated!"
echo
