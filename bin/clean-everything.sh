#!/usr/bin/env sh

# DOCUMENTATION:
# This just basically takes all directories in $PREFIX/cvs and does a make clean
# on the ones that contain a Makefile with a clean rule.

source $HOME/bin/check-env.sh `basename $0`

if [ -z "$MODFILE" ]; then
	MODFILE=$HOME/bin/modules.sh
fi

if [ ! -e $MODFILE ]; then
	echo
	echo WARNING: $MODFILE not found
	echo
	exit 1
fi

echo cd $PREFIX/cvs
cd $PREFIX/cvs

FAILED=""

for mod in *; do
  if [ -e $mod/Makefile ]; then
    if grep '^clean:' $mod/Makefile > /dev/null ; then
	echo -------------------------------------------------------------
	echo Module: $mod
	echo -------------------------------------------------------------

	echo Checking for flags...
	MODCLEANFLAGS=
	if grep CLEANFLAGS_$mod: $MODFILE ; then
		MODCLEANFLAGS=`grep CLEANFLAGS_$mod: $MODFILE | sed -e 's/^[^:]*://'`
	fi

	FLAGFILE=$PREFIX/cvs/APPEND_FLAGS
	echo Checking for append flags in $FLAGFILE...
	if [ -e $FLAGFILE ] ; then
		PERMODCLEANFLAGS=
		if grep CLEANFLAGS_$mod: $FLAGFILE ; then
			PERMODCLEANFLAGS=`grep CLEANFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
		fi
		if [ ! -z "$PERMODCLEANFLAGS" ]; then
			MODCLEANFLAGS="$MODCLEANFLAGS $PERMODCLEANFLAGS"
		fi
	fi

	FLAGFILE=$PREFIX/cvs/OVERRIDE_FLAGS
	echo Checking for override flags in $FLAGFILE...
	if [ -e $FLAGFILE ] ; then
		if grep CLEANFLAGS_$mod: $FLAGFILE ; then
			MODCLEANFLAGS=`grep CLEANFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
		fi
	fi

	echo cd $PREFIX/cvs/$mod
	cd $PREFIX/cvs/$mod

	echo -----------------
	echo Cleaning ...
	echo make $MODCLEANFLAGS clean
	if make $MODCLEANFLAGS clean ; then
		# clean successful, no need to redo cleaning
		echo Cleaned $mod
	else
		echo WARNING:$mod: Cant make clean
		FAILED="$FAILED $mod"
	fi

	echo cd $PREFIX/cvs
	cd $PREFIX/cvs
    fi
  fi
done

if [ x$FAILED = x ]; then
	echo -----------------------
	echo All cleaned successfuly
	echo -----------------------
else
	echo ------------------------------------------------
	echo WARNING: Following modules failed make clean
	echo $FAILED
	echo ------------------------------------------------
fi
