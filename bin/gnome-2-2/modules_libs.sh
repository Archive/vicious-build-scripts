# This is for building the development GNOME libs (from CVS HEAD)

# This file should be sourced in
MODULES='
	esound
	gtk-doc
	glib
	gob
	linc
	atk
	gnome-common
	pango
	libIDL
	ORBit2
	gnome-xml
	intltool
	bonobo-activation
	gtk+
	gconf
	libart_lgpl
	libzvt
	vte
	libbonobo
	gnome-mime-data
	gstreamer
	gst-plugins
	gnome-vfs
	libglade
	libxslt
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	libgnomeprintui
	libgda
	libgnomedb
	libgtop
	librsvg
	gail
	eel
	gtkhtml2
	librep
	rep-gtk
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# CVSFLAGS_eel: -r gnome-2-2
# AUTOGEN_glib: --enable-debug=yes
# CVSFLAGS_glib: -r glib-2-2
# AUTOGEN_gtk+: --enable-debug=yes
# CVSFLAGS_gtk+: -r gtk-2-2
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# CVSFLAGS_gnome-vfs: -r gnome-2-2
# AUTOGEN_libgnome: --enable-compile-warnings=error
# AUTOGEN_libbonobo: --enable-compile-warnings=maximum
# AUTOGEN_libgnomecanvas: --enable-compile-warnings=error
# AUTOGEN_libbonoboui: --enable-compile-warnings=maximum
# CVSFLAGS_libbonoboui: -r gnome-2-2
# AUTOGEN_libgnomeui: --enable-compile-warnings=error
# CVSROOT_gstreamer: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# AUTOGEN_gstreamer: -- --disable-plugin-builddir
# CVSFLAGS_gstreamer: -r BRANCH-GSTREAMER-0_6
# CVSROOT_gst-plugins: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# CVSFLAGS_gst-plugins: -r BRANCH-GSTREAMER-0_6
# CVSFLAGS_ORBit2: -r gnome-2-2
# CVSFLAGS_atk: -r gnome-2-2
# CVSFLAGS_gail: -r gnome-2-2
# CVSFLAGS_gconf: -r gnome-2-2
# CVSFLAGS_gnome-common: -r gnome-2-2
# CVSFLAGS_libgnomeprint: -r gnome-2-2
# CVSFLAGS_libgnomeprintui: -r gnome-2-2
# CVSFLAGS_libwnck: -r gnome-2-2
# CVSFLAGS_linc: -r gnome-2-2
# CVSFLAGS_pango: -r pango-1-2
# CVSFLAGS_vte: -r gnome-2-2
