# This is for building the development GNOME (from CVS HEAD)

# This file should be sourced in

MODULES='
	esound
	gnome-xml
	libxslt
	gtk-doc
	glib
	gob
	linc
	atk
	gnome-common
	pango
	libIDL
	ORBit2
	intltool
	bonobo-activation
	gtk+
	gconf
	libart_lgpl
	libzvt
	vte
	libbonobo
	gnome-mime-data
	gnome-vfs
	gstreamer
	gst-plugins
	libglade
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	libgnomeprintui
	libgda
	libgnomedb
	libgtop
	librsvg
	gail
	eel
	gtkhtml2
	gtk-engines
	gnome-desktop
	gnome-panel
	gnome-session
	gnome-terminal
	gnome-utils
	gnome-applets
        metacity
	gnome-control-center
	glade
	gnome-games
	bug-buddy
	eog
	nautilus	
	procman
	yelp
	gedit
	gnome-media
	gnome-vfs-extras
	thinice2
	gnome-icon-theme
	gnome-themes
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# CVSFLAGS_eel: -r gnome-2-2
# AUTOGEN_glib: --enable-debug=yes
# CVSFLAGS_glib: -r glib-2-2
# AUTOGEN_gtk+: --enable-debug=yes
# CVSFLAGS_gtk+: -r gtk-2-2
# CVSFLAGS_pango: -r pango-1-2
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# AUTOGEN_libgnome: --enable-compile-warnings=error
# AUTOGEN_libbonobo: --enable-compile-warnings=maximum
# AUTOGEN_libgnomecanvas: --enable-compile-warnings=error
# AUTOGEN_libbonoboui: --enable-compile-warnings=maximum
# CVSFLAGS_libbonoboui: -r gnome-2-2
# AUTOGEN_libgnomeui: --enable-compile-warnings=error
# AUTOGEN_libgda: --with-postgres=yes --with-mysql=yes --with-oracle=yes --with-sqlite=yes --with-odbc=yes --with-mdb=yes
# AUTOGEN_sawfish:  --with-gdk-pixbuf
# AUTOGEN_gnumeric: --without-bonobo
# AUTOGEN_ORBit2: --enable-debug
# CVSFLAGS_libgtop: -r libgtop-GNOME-2-0-port
# CVSFLAGS_glade: -r glade-gnome2-branch
# CVSFLAGS_sawfish: -r gnome-2
# CVSROOT_gstreamer: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# AUTOGEN_gstreamer: -- --disable-plugin-builddir
# CVSFLAGS_gstreamer: -r BRANCH-GSTREAMER-0_6
# CVSROOT_gst-plugins: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# CVSFLAGS_gst-plugins: -r BRANCH-GSTREAMER-0_6
# CVSFLAGS_gnome-applets: -r gnome-2-2
# CVSFLAGS_ORBit2: -r gnome-2-2
# CVSFLAGS_atk: -r gnome-2-2
# CVSFLAGS_gail: -r gnome-2-2
# CVSFLAGS_gconf: -r gnome-2-2
# CVSFLAGS_gedit: -r gnome-2-2
# CVSFLAGS_gnome-common: -r gnome-2-2
# CVSFLAGS_gnome-control-center: -r gnome-2-2
# CVSFLAGS_gnome-desktop: -r gnome-2-2
# CVSFLAGS_gnome-games: -r gnome-2-2
# CVSFLAGS_gnome-icon-theme: -r gnome-2-2
# CVSFLAGS_gnome-media: -r gnome-2-2
# CVSFLAGS_gnome-panel: -r gnome-2-2
# CVSFLAGS_gnome-session: -r gnome-2-2
# CVSFLAGS_gnome-terminal: -r gnome-2-2
# CVSFLAGS_gnome-utils: -r gnome-2-2
# CVSFLAGS_gnome-vfs: -r gnome-2-2
# CVSFLAGS_libgnomeprint: -r gnome-2-2
# CVSFLAGS_libgnomeprintui: -r gnome-2-2
# CVSFLAGS_libwnck: -r gnome-2-2
# CVSFLAGS_linc: -r gnome-2-2
# CVSFLAGS_metacity: -r gnome-2-2
# CVSFLAGS_nautilus: -r gnome-2-2
# CVSFLAGS_pango: -r pango-1-2
# CVSFLAGS_procman: -r gnome-2-2
# CVSFLAGS_vte: -r gnome-2-2
# CVSFLAGS_yelp: -r gnome-2-2
# CVSROOT_thinice2: :pserver:anonymous@cvs.sourceforge.net:/cvsroot/thinice
