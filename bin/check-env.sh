# This is sourced in, not to be run separately

if [ -z "$PREFIX" ]; then
	echo
	echo "ERROR: PREFIX not set.  You must set the PREFIX variable "
	echo "       before running $1"
	echo "       Most likely you want to source one of the following files:"
	echo "       ~/bin/gnome-home - George's setup in with PREFIX in \$HOME"
	echo "       ~/bin/gnome-head - CVS HEAD setup in /gnome/head"
	echo "       ~/bin/gnome-head-libs - CVS HEAD setup in /gnome/head"
	echo "       ~/bin/gnome-release-2-0 - Martin's released setup in /gnome/gnome-2-0"
	echo
	exit 1
fi

if [ ! -d "$PREFIX" ]; then
	if ! install -d $PREFIX; then
		echo
		echo "ERROR: $PREFIX doesn't exist and we can't make it"
		echo
		exit 1
	fi
fi

# Lastly set language to en_US since some scripts rely on outputs from
# things and would break
export LANG=en_US
