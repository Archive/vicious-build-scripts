# This is for building the development GNOME (from CVS HEAD)

# This file should be sourced in

MODULES='
	esound
	gnome-xml
	libxslt
	libxklavier
	gtk-doc
	glib
	intltool
	shared-mime-info
	gnome-common
	atk
	pango
	libIDL
	ORBit2
	gtk+
	gconf
	libart_lgpl
	vte
	libbonobo
	gnome-mime-data
	gnome-keyring
	gnome-vfs
	gstreamer
	gst-plugins
	libglade
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	libgnomeprintui
	libgda
	libgnomedb
	libgtop
	libgsf
	libcroco
	librsvg
	gail
	eel
	gtkhtml2
	gtk-engines
	gnome-desktop
	gnome-panel
	gnome-session
	gnome-terminal
	gnome-utils
	gnome-applets
        metacity
	nautilus	
	nautilus-cd-burner
	nautilus-media
	gnome-control-center
	glade
	gnome-games
	bug-buddy
	eog
	procman
	yelp
	gtksourceview
	gedit
	gnome-media
	gnome-icon-theme
	gnome-themes
	at-spi
	libgail-gnome
	at-poke
	gnome-mag
	gnome-speech
	gnopernicus
	acme
	file-roller
	gcalctool
	gconf-editor
	gpdf
	gucharmap
	totem
	zenity
	dasher
	gnome-netstatus
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# AUTOGEN_glib: --enable-debug=yes
# AUTOGEN_gtk+: --enable-debug=yes
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# AUTOGEN_libgnome: --enable-compile-warnings=maximum
# AUTOGEN_libbonobo: --enable-compile-warnings=maximum
# AUTOGEN_libgnomecanvas: --enable-compile-warnings=error
# AUTOGEN_libgnomeprint: --disable-gtk-doc
# AUTOGEN_libgnomeprintui: --disable-gtk-doc
# AUTOGEN_libgsf: --disable-gtk-doc
# AUTOGEN_librsvg: --disable-gtk-doc
# AUTOGEN_libbonoboui: --enable-compile-warnings=maximum
# AUTOGEN_libgnomeui: --enable-compile-warnings=maximum
# AUTOGEN_libgda: --with-postgres=yes --with-mysql=yes --with-oracle=yes --with-sqlite=yes --with-odbc=yes --with-mdb=yes --disable-gtk-doc
# AUTOGEN_ORBit2: --enable-debug
# CVSFLAGS_glade: -r glade-gnome2-branch
# CVSROOT_gstreamer: :pserver:anoncvs@cvs.freedesktop.org:/cvs/gstreamer
# AUTOGEN_gstreamer: -- --disable-plugin-builddir --disable-docs-build
# CVSROOT_gst-plugins: :pserver:anoncvs@cvs.freedesktop.org:/cvs/gstreamer
# AUTOGEN_totem: --enable-gstreamer
# CVSROOT_libxklavier: :pserver:anoncvs@cvs.freedesktop.org:/cvs/xklavier
# CVSROOT_shared-mime-info: :pserver:anoncvs@cvs.freedesktop.org:/cvs/mime
