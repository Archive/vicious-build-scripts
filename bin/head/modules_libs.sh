# This is for building the development GNOME libs (from CVS HEAD)

# This file should be sourced in
MODULES='
	esound
	gtk-doc
	glib
	gnome-common
	atk
	pango
	libIDL
	ORBit2
	gnome-xml
	intltool
	gtk+
	gconf
	libart_lgpl
	vte
	libbonobo
	gnome-mime-data
	gstreamer
	gnome-vfs
	libglade
	libxslt
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	libgnomeprintui
	libgda
	libgnomedb
	libgtop
	libgsf
	libcroco
	librsvg
	gail
	eel
	gtkhtml2
	librep
	rep-gtk
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# AUTOGEN_glib: --enable-debug=yes
# AUTOGEN_gtk+: --enable-debug=yes
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# AUTOGEN_libgnome: --enable-compile-warnings=error
# AUTOGEN_libbonobo: --enable-compile-warnings=maximum
# AUTOGEN_libgnomecanvas: --enable-compile-warnings=error
# AUTOGEN_libbonoboui: --enable-compile-warnings=maximum
# AUTOGEN_libgnomeui: --enable-compile-warnings=error
# CVSROOT_gstreamer: :pserver:anonymous@cvs.gstreamer.sourceforge.net:/cvsroot/gstreamer
# AUTOGEN_gstreamer: -- --disable-plugin-builddir
