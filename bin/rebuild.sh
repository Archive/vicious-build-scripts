#!/usr/bin/env sh

#
# Use following env variables:
#  STARTMODULE=<module>		start with this module
#  AFTERMODULE=<module>		start with the module immediately after this module
#  ENDMODULE=<module>		end with this module
#  CLEAN=yes 			"make distclean" before checking out and
#				building
#  LEAN=yes			after building "make clean"
#  NO_NETWORK=yes		never checkout/update from the network
#  NO_BUILD=yes			never build (useful to just check out all
#				modules quickly)
#  WAITFORUPDATE=yes		Wait for another process to update
#  MARKBUSYALL=yes		Mark all modules as busy updating
#  LOGPOSTFIX=1			Append this to the log file name
#  MODFILE=<modulefile>		alternative module file (default is
#				~/bin/modules.sh)
#  PREFIX=<prefix>		the prefix, must be set
#
#
# The different scripts are:
#   bootstrap.sh		bootstrap an environment, including pulling in
#				autoconf/automake/libtool/gettext
#				You need to set the CVSUSER env variable, unless
#				you are 'jirka' (me) before running this
#   rebuild.sh			Rebuild all modules acording to the above
#				settings
#   modmake.sh <module>		Makes a single modules
#   modules.sh			Default module file, sourcable, sets the
#				MODULES var
#   add-list-to-cvsignore.sh	Utility for maintaining .cvsignore files
#   update-vicious.sh		Update and install scripts from CVS
#

source $HOME/bin/check-env.sh `basename $0`

PRIV_STARTMODULE=$STARTMODULE
PRIV_AFTERMODULE=$AFTERMODULE

if [ ! -z $PRIV_STARTMODULE ] ; then
	PRIV_AFTERMODULE=
fi

if [ -z "$MODFILE" ]; then
	MODFILE=$HOME/bin/modules.sh
fi

if [ ! -e $MODFILE ]; then
	echo
	echo WARNING: $MODFILE not found
	echo
	exit 1
fi

# Make sure it's in the environment
export MODFILE PREFIX

function make_line () {
	echo "==============================================================================="
}

make_line >> $PREFIX/build$LOGPOSTFIX.log
echo GNOME rebuild.sh build log `date`>> $PREFIX/build$LOGPOSTFIX.log
echo >> $PREFIX/build$LOGPOSTFIX.log

function make_logend () {
	echo
	echo "Build log finished! "`date`
	make_line
}

if [ ! -z "$MODULES" ]; then
	echo MODULES variable found, using that instead of sourcing $MODFILE
else
	echo source $MODFILE
	source $MODFILE
fi

echo cd $PREFIX/cvs
cd $PREFIX/cvs
echo source setvars.sh
source setvars.sh

# if we have alternative CVSROOTs somewhere
if grep -q CVSROOT $MODFILE ; then
	PRIV1_STARTMODULE=$PRIV_STARTMODULE
	PRIV1_AFTERMODULE=$PRIV_AFTERMODULE
	for mod in $MODULES ; do

		if [ x$ENDMODULE = x$mod ]; then
			break
		fi
	
		if [ ! -z "$PRIV1_STARTMODULE" ]; then
			if [ x$PRIV1_STARTMODULE = x$mod ]; then
				PRIV1_STARTMODULE=
			else
				continue
			fi
		else if [ ! -z $PRIV1_AFTERMODULE ]; then
			if [ x$PRIV1_AFTERMODULE = x$mod ] ; then
				PRIV1_AFTERMODULE=
			fi
			continue
		fi fi

		MODCVSROOT=
		if grep CVSROOT_$mod: $MODFILE ; then
			MODCVSROOT=`grep CVSROOT_$mod: $MODFILE | sed -e 's/^[^:]*:[ 	]*//'`
		fi
		FLAGFILE=$PREFIX/cvs/APPEND_FLAGS
		if [ -e $FLAGFILE ] ; then
			if grep CVSROOT_$mod: $FLAGFILE ; then
				MODCVSROOT=`grep CVSROOT_$mod: $FLAGFILE | sed -e 's/^[^:]*:[ 	]*//'`
			fi
		fi

		FLAGFILE=$PREFIX/cvs/OVERRIDE_FLAGS
		if [ -e $FLAGFILE ] ; then
			if grep CVSROOT_$mod: $FLAGFILE ; then
				MODCVSROOT=`grep CVSROOT_$mod: $FLAGFILE | sed -e 's/^[^:]*:[ 	]*//'`
			fi
		fi
		if [ ! -z "$MODCVSROOT" ]; then
			if ! fgrep -q `echo $MODCVSROOT | sed 's/:[^:]*$//'` ~/.cvspass; then
				echo '********************************'
				echo '********************************'
				echo '********************************'
				echo Found a CVS server that you have not logged into
				echo if this is an anonymous cvs server usually just
				echo press enter to continue.
				echo
				echo "CVSROOT=$MODCVSROOT $CVSPREFIX cvs login"
				CVSROOT=$MODCVSROOT $CVSPREFIX cvs login
			fi
		fi
	done
fi

if [ x$MARKBUSYALL = xyes ]; then
	PRIV1_STARTMODULE=$PRIV_STARTMODULE
	PRIV1_AFTERMODULE=$PRIV_AFTERMODULE
	for mod in $MODULES ; do

		if [ x$ENDMODULE = x$mod ]; then
			break
		fi
	
		if [ ! -z "$PRIV1_STARTMODULE" ]; then
			if [ x$PRIV1_STARTMODULE = x$mod ]; then
				PRIV1_STARTMODULE=
			else
				continue
			fi
		else if [ ! -z $PRIV1_AFTERMODULE ]; then
			if [ x$PRIV1_AFTERMODULE = x$mod ] ; then
				PRIV1_AFTERMODULE=
			fi
			continue
		fi fi

		touch $PREFIX/waiting-on-update-$mod
	
	done
fi

for mod in $MODULES ; do

	if [ x$ENDMODULE = x$mod ]; then
		echo
		echo Ending with $mod ...
		echo
		break
	fi
	
	if [ ! -z "$PRIV_STARTMODULE" ]; then
		if [ x$PRIV_STARTMODULE = x$mod ]; then
			PRIV_STARTMODULE=
		else
			echo Skipping $mod ...
			continue
		fi
	else if [ ! -z $PRIV_AFTERMODULE ]; then
		if [ x$PRIV_AFTERMODULE = x$mod ] ; then
			PRIV_AFTERMODULE=
		fi
		echo Skipping $mod ...
		continue
	fi fi

	echo Building $mod ... >> $PREFIX/build$LOGPOSTFIX.log

	echo
	echo -----------------
	echo Running:
	echo $HOME/bin/modmake.sh $mod
	echo
	if ! $HOME/bin/modmake.sh $mod ; then
		echo
		echo Build failed on $mod
		echo
		echo "failed!" >> $PREFIX/build$LOGPOSTFIX.log
		make_logend >> $PREFIX/build$LOGPOSTFIX.log
		exit 1
	fi

	echo "done!" >> $PREFIX/build$LOGPOSTFIX.log

done

echo
echo -----------------
echo All done!
echo

make_logend >> $PREFIX/build$LOGPOSTFIX.log
