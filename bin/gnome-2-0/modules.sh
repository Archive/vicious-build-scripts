# This is for building GNOME 2.0.x releases.

# This file should be sourced in

MODULES='
	esound
	gnome-xml
	libxslt
	gtk-doc
	glib
	gob
	linc
	atk
	gnome-common
	pango
	libIDL
	ORBit2
	intltool
	bonobo-activation
	gtk+
	gconf
	libart_lgpl
	libzvt
	libbonobo
	gnome-mime-data
	gnome-vfs
	libglade
	libgnome
	libgnomecanvas
	libbonoboui
	libgnomeui
	libwnck
	libgnomeprint
	libgnomeprintui
	libgtop
	librsvg
	gail
	eel
	gtkhtml2
	gtk-engines
	gnome-desktop
	gnome-panel
	gnome-session
	gnome-terminal
	gnome-utils
	gnome-applets
	gnome-control-center
	glade
	gnome-games
	bug-buddy
	eog
	nautilus	
	procman
	yelp
	gedit
	librep
	rep-gtk
	sawfish
	metacity
	gnome-media
	gnome-vfs-extras
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# AUTOGEN_glib: --enable-debug=yes
# AUTOGEN_gtk+: --enable-debug=yes
# AUTOGEN_gnome-vfs: --enable-platform-gnome-2
# AUTOGEN_libgnome: --enable-compile-warnings=error
# AUTOGEN_libbonobo: --enable-compile-warnings=maximum
# AUTOGEN_libgnomecanvas: --enable-compile-warnings=error
# AUTOGEN_libbonoboui: --enable-compile-warnings=maximum
# AUTOGEN_libgnomeui: --enable-compile-warnings=error
# AUTOGEN_libgda: --with-postgres=yes --with-mysql=yes --with-oracle=yes --with-sqlite=yes --with-odbc=yes --with-mdb=yes
# AUTOGEN_sawfish:  --with-gdk-pixbuf
# AUTOGEN_gnumeric: --without-bonobo
# AUTOGEN_ORBit2: --enable-debug
# CVSFLAGS_ORBit2: -r gnome-2-0
# CVSFLAGS_linc: -r gnome-2-0
# CVSFLAGS_bonobo-activation: -r gnome-2-0
# CVSFLAGS_gnome-common: -r gnome-2-0
# CVSFLAGS_glib: -r glib-2-0
# CVSFLAGS_pango: -r pango-1-0
# CVSFLAGS_gtk+: -r gtk-2-0
# CVSFLAGS_gconf: -r gconf-1-2
# CVSFLAGS_gnome-vfs: -r gnome-2-0
# CVSFLAGS_libgnome: -r gnome-2-0
# CVSFLAGS_libgnomecanvas: -r gnome-2-0
# CVSFLAGS_libgnomeui: -r gnome-2-0
# CVSFLAGS_libgnomeprint: -r gnome-2-0
# CVSFLAGS_libgnomeprintui: -r gnome-2-0
# CVSFLAGS_gnome-desktop: -r gnome-2-0
# CVSFLAGS_gnome-panel: -r gnome-2-0
# CVSFLAGS_gnome-session: -r gnome-2-0
# CVSFLAGS_gnome-games: -r gnome-2-0
# CVSFLAGS_gnome-utils: -r gnome-2-0
# CVSFLAGS_gnome-applets: -r gnome-2-0
# CVSFLAGS_gnome-control-center: -r gnome-2-0
# CVSFLAGS_yelp: -r gnome-2-0
# CVSFLAGS_gnome-terminal: -r gnome-2-0
# CVSFLAGS_bug-buddy: -r gnome-2-0
# CVSFLAGS_eog: -r gnome-2-0
# CVSFLAGS_libgtop: -r libgtop-GNOME-2-0-port
# CVSFLAGS_glade: -r glade-gnome2-branch
# CVSFLAGS_sawfish: -r gnome-2
# CVSFLAGS_gtkhtml2: -r gnome-2-0
# CVSFLAGS_gconf-editor: -r gnome-2-0
# CVSFLAGS_gedit: -r gnome-2-0
# CVSFLAGS_gail: -r gnome-2-0
# CVSFLAGS_libwnck: -r gnome-2-0
# CVSFLAGS_eel: -r gnome-2-0
# CVSFLAGS_nautilus: -r gnome-2-0
