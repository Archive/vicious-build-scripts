#!/usr/bin/env zsh

# DOCS:
# Takes a list of files to add to .cvsignores with path relative to current dir
# The way I use this is to run cvs update, then take the list of files with
# questionmarks put them in a file edit the file appropriately and then run
# add-list-to-cvsignore.sh `cat file`

for n in "$@" ; do
	DIRNAME=`dirname $n`
	BASENAME=`basename $n`
	IGNOREFILE=$DIRNAME/.cvsignore
	if [ -e $IGNOREFILE ] ; then
		echo $BASENAME >> $IGNOREFILE
	else
		echo $BASENAME >> $IGNOREFILE
		$CVSPREFIX cvs -z3 add $IGNOREFILE
	fi
done
