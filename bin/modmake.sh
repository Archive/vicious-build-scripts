#!/usr/bin/env sh

source $HOME/bin/check-env.sh `basename $0`

if [ -z "$1" ]; then
	echo "`basename $0` MUST get one argument and thats the module to build"
	exit 1
fi

mod=$1

if [ x$NO_NETWORK = xyes ]; then
	if [ x$WAITFORUPDATE = xyes ] ; then
		while [ -f $PREFIX/waiting-on-update-$mod ]; do
			echo "..."
			echo "Waiting for $mod to update. (sleeping 2 seconds)"
			echo "If this is wrong please remove the $PREFIX/waiting-on-update-$mod file"
			sleep 2
		done
	fi
fi

if [ x$MARKBUSYALL != xyes ]; then
	rm -f $PREFIX/waiting-on-update-$mod
fi

if [ x$KILLGCONF = xyes ]; then
	echo -----------------
	echo Following might just blow up, nothing to worry about
	echo Shutting down gconf-1...
	echo killall gconfd-1
	killall gconfd-1
	echo Shutting down gconf-2...
	echo killall gconfd-2
	killall gconfd-2
	echo Shutting down gconf in general...
	echo killall gconfd
	killall gconfd
	echo -----------------
fi

if [ -z "$MODFILE" ]; then
	MODFILE=$HOME/bin/modules.sh
fi

if [ ! -e $MODFILE ]; then
	echo
	echo WARNING: $MODFILE not found
	echo
	exit 1
fi

echo cd $PREFIX/cvs
cd $PREFIX/cvs
source setvars.sh

echo -------------------------------------------------------------
echo Module: $mod
echo -------------------------------------------------------------

echo Checking for flags...
MODFLAGS=
if grep AUTOGEN_$mod: $MODFILE ; then
	MODFLAGS=`grep AUTOGEN_$mod: $MODFILE | sed -e 's/^[^:]*://'`
	MODFLAGS=`eval "echo $MODFLAGS"`
fi
MODCVSFLAGS=
if grep CVSFLAGS_$mod: $MODFILE ; then
	MODCVSFLAGS=`grep CVSFLAGS_$mod: $MODFILE | sed -e 's/^[^:]*://'`
fi
MODCLEANFLAGS=
if grep CLEANFLAGS_$mod: $MODFILE ; then
	MODCLEANFLAGS=`grep CLEANFLAGS_$mod: $MODFILE | sed -e 's/^[^:]*://'`
fi
MODCFLAGS=
if grep CFLAGS_$mod: $MODFILE ; then
	MODCFLAGS=`grep CFLAGS_$mod: $MODFILE | sed -e 's/^[^:]*://'`
fi
MODCVSROOT=
if grep CVSROOT_$mod: $MODFILE ; then
	MODCVSROOT=`grep CVSROOT_$mod: $MODFILE | sed -e 's/^[^:]*:[ 	]*//'`
fi

FLAGFILE=$PREFIX/cvs/APPEND_FLAGS
echo Checking for append flags in $FLAGFILE...
if [ -e $FLAGFILE ] ; then
	PERMODFLAGS=
	if grep AUTOGEN_$mod: $FLAGFILE ; then
		PERMODFLAGS=`grep AUTOGEN_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
		PERMODFLAGS=`eval "echo $PERMODFLAGS"`
	fi
	if [ ! -z "$PERMODFLAGS" ]; then
		MODFLAGS="$MODFLAGS $PERMODFLAGS"
	fi
	PERMODCVSFLAGS=
	if grep CVSFLAGS_$mod: $FLAGFILE ; then
		PERMODCVSFLAGS=`grep CVSFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
	fi
	if [ ! -z "$PERMODCVSFLAGS" ]; then
		MODCVSFLAGS="$MODCVSFLAGS $PERMODCVSFLAGS"
	fi
	PERMODCLEANFLAGS=
	if grep CLEANFLAGS_$mod: $FLAGFILE ; then
		PERMODCLEANFLAGS=`grep CLEANFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
	fi
	if [ ! -z "$PERMODCLEANFLAGS" ]; then
		MODCLEANFLAGS="$MODCLEANFLAGS $PERMODCLEANFLAGS"
	fi
	PERMODCFLAGS=
	if grep CFLAGS_$mod: $FLAGFILE ; then
		PERMODCFLAGS=`grep CFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
	fi
	if [ ! -z "$PERMODCFLAGS" ]; then
		MODCFLAGS="$MODCFLAGS $PERMODCFLAGS"
	fi

	# CVSROOT is always just overriden
	if grep CVSROOT_$mod: $FLAGFILE ; then
		MODCVSROOT=`grep CVSROOT_$mod: $FLAGFILE | sed -e 's/^[^:]*:[ 	]*//'`
	fi
fi

FLAGFILE=$PREFIX/cvs/OVERRIDE_FLAGS
echo Checking for override flags in $FLAGFILE...
if [ -e $FLAGFILE ] ; then
	if grep AUTOGEN_$mod: $FLAGFILE ; then
		MODFLAGS=`grep AUTOGEN_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
		MODFLAGS=`eval "echo $PERMODFLAGS"`
	fi
	if grep CVSFLAGS_$mod: $FLAGFILE ; then
		MODCVSFLAGS=`grep CVSFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
	fi
	if grep CLEANFLAGS_$mod: $FLAGFILE ; then
		MODCLEANFLAGS=`grep CLEANFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
	fi
	if grep CFLAGS_$mod: $FLAGFILE ; then
		MODCFLAGS=`grep CFLAGS_$mod: $FLAGFILE | sed -e 's/^[^:]*://'`
	fi
	if grep CVSROOT_$mod: $FLAGFILE ; then
		MODCVSROOT=`grep CVSROOT_$mod: $FLAGFILE | sed -e 's/^[^:]*:[ 	]*//'`
	fi
fi

if [ -z "$MODCVSFLAGS" ]; then
	MODCVSFLAGS="-A"
fi

if [ ! -z "$MODCFLAGS" ]; then
	CFLAGS="$CFLAGS $MODCFLAGS"
	export CFLAGS
fi

if [ ! -z "$CFLAGS" ]; then
	echo "CFLAGS=$CFLAGS"
fi

if [ ! -z "$MODCVSROOT" ]; then
	export CVSROOT=$MODCVSROOT
	if ! fgrep -q `echo $MODCVSROOT | sed 's/:[^:]*$//'` ~/.cvspass; then
		echo "$CVSPREFIX cvs login"
		$CVSPREFIX cvs login
	fi
fi

MODCLEAN=$CLEAN
echo cd $PREFIX/cvs
cd $PREFIX/cvs
if [ ! -d $mod ]; then
	if [ x$NO_NETWORK = xyes ]; then
		echo
		echo "ERROR:$mod: Module doesn't exist, and no network"
		echo
		exit 1
	fi
	touch $PREFIX/waiting-on-update-$mod
	echo "$CVSPREFIX cvs -z3 co $MODCVSFLAGS $mod"
	if ! $CVSPREFIX cvs -z3 co $MODCVSFLAGS $mod ; then
		echo
		echo ERROR:$mod: checking out $mod
		echo
		exit 1
	fi
	rm -f $PREFIX/waiting-on-update-$mod
	# definately clean already
	MODCLEAN=no
	echo cd $PREFIX/cvs/$mod
	cd $PREFIX/cvs/$mod
else
	echo cd $PREFIX/cvs/$mod
	cd $PREFIX/cvs/$mod
	if [ -e CLEAN ]; then
		MODCLEAN=yes
		rm -f CLEAN
	fi
	if [ x$MODCLEAN = xyes ]; then
		echo -----------------
		echo Cleaning before update...
		echo rm -f config.cache
		rm -f config.cache
		echo make $MODCLEANFLAGS distclean
		if make $MODCLEANFLAGS distclean ; then
			# clean successful, no need to redo cleaning
			MODCLEAN=no
		else
			echo WARNING:$mod: Cant make distclean before update
		fi
	fi
	if [ ! x$NO_NETWORK = xyes ]; then
		touch $PREFIX/waiting-on-update-$mod
		echo $CVSPREFIX cvs -z3 update -dP $MODCVSFLAGS
		if ! $CVSPREFIX cvs -z3 update -dP $MODCVSFLAGS ; then
			echo
			echo ERROR:$mod: updating $mod
			echo
			exit 1
		fi
		rm -f $PREFIX/waiting-on-update-$mod
	fi
fi

#
# NO_BUILD=yes will make things not build but just check-out
#
if [ x$NO_BUILD = xyes ]; then
	exit 0
fi

if [ ! x$NO_CONFIGURE = xyes -o ! -e Makefile ]; then
	echo -----------------
	echo Configuring...
	echo rm -f Makefile
	rm -f Makefile
	if echo -- $MODFLAGS | grep -q -- --prefix ; then
		echo ./autogen.sh $MODFLAGS
		./autogen.sh $MODFLAGS
	else
		# Modflags must come first since they might contain
		# things that are necessary to pass options to configure
		# such as for gstreamer we need to have -- in the options
		echo ./autogen.sh $MODFLAGS --prefix=$PREFIX/INSTALL 
		./autogen.sh $MODFLAGS --prefix=$PREFIX/INSTALL
	fi
	if [ ! -e Makefile ]; then
		echo
		echo ERROR:$mod: Cant autogen
		echo
		exit 1
	fi
fi

if [ x$MODCLEAN = xyes ]; then
	if [ -e Makefile ]; then
		echo -----------------
		echo Cleaning...
		echo rm -f config.cache
		rm -f config.cache
		echo make $MODCLEANFLAGS distclean
		if ! make $MODCLEANFLAGS distclean ; then
			echo
			echo ERROR:$mod: Cant make distclean
			echo
			exit 1
		fi

		echo -----------------
		echo Configuring again...
		echo rm -f Makefile
		rm -f Makefile
		echo ./autogen.sh --prefix=$PREFIX/INSTALL $MODFLAGS
		./autogen.sh --prefix=$PREFIX/INSTALL $MODFLAGS
		if [ ! -e Makefile ]; then
			echo
			echo ERROR:$mod: Cant autogen
			echo
			exit 1
		fi
	fi
fi


echo -----------------
echo Running make all

echo make all
if ! make all ; then
	echo
	echo WARNING:$mod: Cant make all
	echo
	echo rm -f Makefile
	rm -f Makefile
	echo rm -f config.cache
	rm -f config.cache
	echo -----------------
	echo Configuring
	echo ./autogen.sh --prefix=$PREFIX/INSTALL $MODFLAGS
	./autogen.sh --prefix=$PREFIX/INSTALL $MODFLAGS
	if [ ! -e Makefile ]; then
		echo
		echo ERROR:$mod: Cant autogen
		echo
		exit 1
	fi
	echo make all
	if ! make all ; then
		echo
		echo ERROR:$mod: Cant make all
		echo
		exit 1
	fi
fi

echo -----------------
echo Installing

echo make install
if ! make install ; then
	echo
	echo ERROR:$mod: Cant install
	echo
	exit 1
fi

if [ x$LEAN = xyes ]; then
	echo -----------------
	echo Lean clean
	echo make $MODCLEANFLAGS clean
	make $MODCLEANFLAGS clean
fi
