#!/usr/bin/env sh

if [ -z "$CVSUSER" ]; then
	CVSUSER=jirka
	if [ ! -e /home/jirka ]; then
		echo
		echo "WARNING: CVSUSER wasn't set. I'm assuming you want to use anoncvs."
		echo "       Run bootstrap.sh as follows to get rid of the warning.:"
		echo "       CVSUSER=[gnomecvs user name|anonymous] bootstrap.sh"
		echo
		CVSUSER=anonymous
	fi
fi

source $HOME/bin/check-env.sh `basename $0`

echo cd $PREFIX
cd $PREFIX
if [ ! -d cvs ]; then
	echo mkdir cvs
	if ! mkdir cvs; then
		echo '***'
		echo "Can't make $PREFIX/cvs directory"
		echo '***'
	fi
fi
echo cd cvs
cd cvs

if [ $CVSUSER = "anonymous" ]; then
	sed -e "s/@CVSUSER@/$CVSUSER/" > setvars.sh <<'EOF'
	CVSROOT=':pserver:anonymous@anoncvs4.gnome.org:/cvs/gnome'
	export CVSROOT
	if ! fgrep -q anoncvs4.gnome.org ~/.cvspass; then
		$CVSPREFIX cvs login
	fi
EOF
else
	sed -e "s/@CVSUSER@/$CVSUSER/" > setvars.sh <<'EOF'
	CVSROOT=':pserver:@CVSUSER@@cvs.gnome.org:/cvs/gnome'
	export CVSROOT
	if ! fgrep -q cvs.gnome.org ~/.cvspass; then
		$CVSPREFIX cvs login
	fi
EOF
fi
source setvars.sh

echo
echo "getting/making autoconf/automake/gettext/libtool/pkgconfig/audiofile/fontconf/xrender/xft"
echo

export PATH=$PREFIX/INSTALL/bin:$PATH
export LD_LIBRARY_PATH=$PREFIX/INSTALL/lib:$LD_LIBRARY_PATH

echo mkdir $PREFIX/INSTALL
mkdir $PREFIX/INSTALL

echo cd $PREFIX
cd $PREFIX

if [ -z "$VICIOUS_BOOTSTRAPS" ] ; then
    # Note: gnome-home and gnome-release-2-0 defines VICIOUS_BOOTSTRAPS to be
    # different.  Make sure to update those as well when updating these.
    BOOTSTRAPS='
    	http://www.ibiblio.org/pub/gnu/autoconf/autoconf-2.57.tar.gz
	http://www.ibiblio.org/pub/gnu/automake/automake-1.4-p6.tar.gz
	http://www.ibiblio.org/pub/gnu/automake/automake-1.7.9.tar.bz2
	http://www.ibiblio.org/pub/gnu/gettext/gettext-0.12.1.tar.gz
	http://www.ibiblio.org/pub/gnu/libtool/libtool-1.5.tar.gz
	http://www.freedesktop.org/software/pkgconfig/releases/pkgconfig-0.15.0.tar.gz
	ftp://oss.sgi.com/projects/audiofile/download/audiofile-0.2.3.tar.gz
        http://pdx.freedesktop.org/software/fontconfig/releases/xrender-0.8.3.tar.gz
        http://pdx.freedesktop.org/software/fontconfig/releases/fontconfig-2.2.0.tar.gz
        http://pdx.freedesktop.org/software/fontconfig/releases/xft-2.1.2.tar.gz
	http://www.freedesktop.org/software/startup-notification/releases/startup-notification-0.5.tar.gz
    '
else
    BOOTSTRAPS="$VICIOUS_BOOTSTRAPS"
fi

for n in $BOOTSTRAPS; do
	BASENAME=`basename $n`
	echo 
	echo Getting $BASENAME
	echo 
	if [ ! -e $BASENAME ]; then
		wget -c --passive-ftp $n
	fi
	if [ ! -e $BASENAME ]; then
		echo '***'
		echo "Cant get $BASENAME"
		echo '***'
		exit 1
	fi
done

for n in $BOOTSTRAPS; do
	BASENAME=`basename $n`
	NAME=${BASENAME%.tar.gz}
	FLAGS="xzvf"
	if [ x$NAME = x$BASENAME ]; then
		NAME=${NAME%.tar.bz2}
		FLAGS="xjvf"
	fi

	echo 
	echo Building $NAME
	echo 

	echo cd $PREFIX
	cd $PREFIX

	echo tar $FLAGS $BASENAME
	tar $FLAGS $BASENAME

	echo cd $NAME
	if ! cd $NAME ; then
		echo '***'
		echo "ERROR: Can't cd into $NAME"
		echo '***'
		exit 1
	fi

	SUBDIRS=.
	EXTRA_FLAGS=
	
	if [ -e fontconfig ]; then
		EXTRA_FLAGS=--disable-docs
	fi


	for s in $SUBDIRS; do
		echo cd $s
		if ! cd $s ; then
			echo '***'
			echo "ERROR: Can't cd into $NAME/$s"
			echo '***'
			exit 1
		fi

		echo ./configure --prefix=$PREFIX/INSTALL $EXTRA_FLAGS
		./configure --prefix=$PREFIX/INSTALL $EXTRA_FLAGS

		echo make
		if ! make; then
			echo '***'
			echo "ERROR: Can't make $NAME"
			echo '***'
			exit 1
		fi
		
		echo make install
		if ! make install; then
			echo '***'
			echo "ERROR: Can't make install $NAME"
			echo '***'
			exit 1
		fi

		echo cd ..
		cd ..
	done

	echo cd $PREFIX
	cd $PREFIX

	echo rm -fR $NAME
	rm -fR $NAME
done

echo cd $PREFIX/INSTALL
cd $PREFIX/INSTALL

echo
echo "Bootstrapped ok!"
echo
echo "Make sure you always source the correct enviroment setup"
echo "for $PREFIX before you start working"
echo
echo " Note: To use cvs, you must source in the setvars.sh in"
echo "       your $PREFIX/cvs directory first."
echo
echo " All done"
echo

