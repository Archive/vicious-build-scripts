#!/usr/bin/env zsh

source $HOME/bin/check-env.sh ${0:t}

if [ -z "$1" ]; then
	echo "$0 MUST get one argument and thats the module to configure"
	exit 1
fi

mod=$1

if [ -z "$MODFILE" ]; then
	MODFILE=$HOME/bin/modules.sh
fi

if [ ! -e $MODFILE ]; then
	echo
	echo WARNING: $MODFILE not found
	echo
	exit 1
fi

echo -------------------------------------------------------------
echo Configuring in $PWD for module $mod
echo -------------------------------------------------------------

echo Checking for flags...
MODFLAGS=
if grep AUTOGEN_$mod $MODFILE ; then
	MODFLAGS=`grep AUTOGEN_$mod: $MODFILE | sed -e 's/^[^:]*://'`
	MODFLAGS=`eval "echo $MODFLAGS"`
fi


echo -----------------
echo Configuring...
echo rm -f config.cache
rm -f config.cache
echo ./autogen.sh --prefix=$PREFIX/INSTALL ${=MODFLAGS}
./autogen.sh --prefix=$PREFIX/INSTALL ${=MODFLAGS}
if [ ! -e Makefile ]; then
	echo
	echo ERROR:$mod: Cant autogen
	echo
	exit 1
fi
