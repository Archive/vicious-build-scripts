# This is for building the GNOME 1.4 packages.
# For GNOME 2.0 you probably want to use head/modules.sh.

# This file should be sourced in

MODULES='
	intltool
	gnome-common
	gtk-doc
	esound
	glib
	libunicode
	gtk+
	ORBit
	gob
	gnome-xml
	libgtop
	oaf
	gnet
	gnome-http
	gconf
	imlib
	gnome-libs
	gdk-pixbuf
	gnome-print
	bonobo
	gnome-vfs
	control-center
	libglade
	gal
	gtkhtml
	gnome-core
	gnome-applets
	pong
	grapevine
	eog
	medusa
	ammonite
	librsvg
	eel
	nautilus
	gnome-utils
'

# This is commented but is grepped out later
# thus this file always needs to be ~/bin/modules.sh
#
# CVSFLAGS_ORBit:-rorbit-stable-0-5
# CLEANFLAGS_ORBit:-k
# AUTOGEN_bonobo: --enable-oaf=yes
# CVSFLAGS_control-center: -rcontrol-center-1-0
# AUTOGEN_eog: --enable-oaf=yes
# CVSFLAGS_glib: -rglib-1-2
# AUTOGEN_glib: --enable-debug=yes
# CVSFLAGS_gnome-core: -rgnome-core-1-4
# CVSFLAGS_gnome-applets: -rgnome-applets-1-4
# CVSFLAGS_gnome-libs: -rgnome-libs-1-0
# AUTOGEN_gnome-libs: --enable-prefer-db1
# CVSFLAGS_gnome-print: -rgnome-1-4-branch
# AUTOGEN_gnome-vfs: --enable-oaf=yes
# CVSFLAGS_gnome-vfs: -rgnome-vfs-1-0
# CVSFLAGS_gnome-xml: -rLIB_XML_1_BRANCH
# CVSFLAGS_gtk+: -rgtk-1-2
# AUTOGEN_gtk+: --enable-debug=yes
# CVSFLAGS_gtk--: -rgtkmm_1_2_0
# AUTOGEN_libglade: --with-gnome
# CVSFLAGS_libglade: -rlibglade-1-0
# CVSFLAGS_libsigc++: -rsigc_1_0_0
# AUTOGEN_nautilus: --enable-eazel-services --enable-more-warnings
# CFLAGS_nautilus: -g -O0 -DENABLE_SCROLLKEEPER_SUPPORT
# CVSFLAGS_libgtop: -rLIBGTOP_STABLE_1_0
# CVSFLAGS_gconf: -rgconf-1-0
# CVSFLAGS_gob: -rgob-1-0
# CVSFLAGS_pong: -rpong-1-0
# CVSFLAGS_gnome-utils: -rgnome-utils-1-4
