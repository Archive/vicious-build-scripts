all:
	@echo "Vicious Build Scripts:"
	@echo
	@echo "Run:"
	@echo "\"make install\" to install"
	@echo "\"make dist\" to make a tarball"
	@echo

install:
	cd bin && make install

FILENAME=vicious-build-scripts-`LANG=en_US date +'%m%d%Y'`

dist:
	-rm -fR $(FILENAME)
	mkdir $(FILENAME)
	mkdir $(FILENAME)/bin
	mkdir $(FILENAME)/bin/head
	mkdir $(FILENAME)/bin/gnome-2-0
	mkdir $(FILENAME)/bin/gnome-2-2
	cp -a \
		ChangeLog			\
		Makefile			\
		README				\
		HACKING				\
		$(FILENAME)
	cp -a \
		bin/Makefile			\
		bin/add-list-to-cvsignore.sh	\
		bin/bootstrap.sh		\
		bin/clean-everything.sh		\
		bin/install			\
		bin/install-hack		\
		bin/modmake.sh			\
		bin/runconf.sh			\
		bin/modules.sh			\
		bin/rebuild.sh			\
		bin/update-vicious.sh		\
		bin/check-env.sh		\
		bin/gnome-home			\
		bin/gnome-head			\
		bin/gnome-head-libs		\
		bin/gnome-release-2-0		\
		bin/gnome-release-2-2		\
		bin/getlog			\
		bin/vbs-aliases			\
		bin/makelog			\
		bin/addlog			\
		bin/newlog			\
		$(FILENAME)/bin
	cp -a bin/head/modules.sh $(FILENAME)/bin/head
	cp -a bin/head/modules_libs.sh $(FILENAME)/bin/head
	cp -a bin/gnome-2-0/modules.sh $(FILENAME)/bin/gnome-2-0
	cp -a bin/gnome-2-2/modules.sh $(FILENAME)/bin/gnome-2-0
	cp -a bin/gnome-2-2/modules_libs.sh $(FILENAME)/bin/gnome-2-0
	tar czvf $(FILENAME).tar.gz $(FILENAME)
	-rm -fR $(FILENAME)
	@banner="$(FILENAME).tar.gz is ready for distribution"; \
	dashes=`echo "$$banner" | sed s/./=/g`; \
	echo; \
	echo "$$dashes"; \
	echo "$$banner"; \
	echo "$$dashes"
